<?php

/**
 * @file
 * Contains alter_hreflang.module.
 */

use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Url;

/**
 * Implements hook_page_attachments_alter().
 */
function alter_hreflang_page_attachments_alter(array &$attachments) {
  // No need to add hreflang tags for 404/403 pages.
  if (\Drupal::request()->attributes->has('exception')) {
    return;
  }
  $language_manager = \Drupal::getContainer()->get('language_manager');
  // A hreflang for single language site.
  if (!$language_manager->isMultilingual()) {
    $current_path = \Drupal::service('path.current')->getPath();
    $current_url = \Drupal::service('path_alias.manager')
      ->getAliasByPath($current_path);
    global $base_url;
    $page_url = $base_url . $current_url;
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    // Override default hreflang with custom hreflang for site.
    $config = \Drupal::config('alter_hreflang.settings');
    $custom_langcode = $config->get('language_' . $langcode);
    $attachments['#attached']['html_head_link'][] = [
      [
        'rel' => 'alternate',
        'hreflang' => (isset($custom_langcode) && !empty($custom_langcode)) ? $custom_langcode : $langcode,
        'href' => $page_url,
      ],
      TRUE,
    ];
  }
  else {
    $route = \Drupal::service('path.matcher')
      ->isFrontPage() ? '<front>' : '<current>';
    $links = $language_manager->getLanguageSwitchLinks(LanguageInterface::TYPE_INTERFACE, Url::fromRoute($route));
    if (empty($links->links)) {
      return;
    }
    $node = \Drupal::routeMatch()->getParameter('node');
    $config = \Drupal::config('alter_hreflang.settings');
    $enanledlang = array_keys($node->getTranslationLanguages());
    foreach ($links->links as $langcode => $link) {
      if(in_array($langcode, $enanledlang)){
        // Override default hreflang with custom hreflang for site.
        $custom_langcode = $config->get('language_' . $langcode);
        if (!isset($link['query'])) {
          $link['query'] = [];
        }
        $link['query'] += \Drupal::request()->query->all();
        $link['url']->setOptions($link);
        $link['url']->setAbsolute();
        $attachments['#attached']['html_head_link'][] = [
          [
            'rel' => 'alternate',
            'hreflang' => (isset($custom_langcode) && !empty($custom_langcode)) ? $custom_langcode : $langcode,
            'href' => $link['url']->toString(),
          ],
          TRUE,
        ];
      }
    }
  }
}
